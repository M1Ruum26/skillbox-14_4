﻿#include <iostream>
#include <string>

int main()
{
    std::string string =  "Hello Skillbox";
    std::cout << string << "\n";
    std::cout << "Lenght " << string.size() << "\n";
    std::cout << "First character " << string.front() << "\n";
    std::cout << "Last character " << string.back() << "\n";
}

